# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2015, 2016, 2018, 2020, 2021.
# JungHee Lee <daemul72@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2021-05-16 13:54+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 20.12.3\n"

#: package/contents/ui/MinimizeAllController.qml:17
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Minimize All Windows"
msgctxt "@action:button"
msgid "Restore All Minimized Windows"
msgstr "모든 창 최소화"

#: package/contents/ui/MinimizeAllController.qml:18
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Minimize All Windows"
msgctxt "@action:button"
msgid "Minimize All Windows"
msgstr "모든 창 최소화"

#: package/contents/ui/MinimizeAllController.qml:20
#, kde-format
msgctxt "@info:tooltip"
msgid "Restores the previously minimized windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:21
#, fuzzy, kde-format
#| msgid "Show the desktop by moving windows aside"
msgctxt "@info:tooltip"
msgid "Shows the Desktop by minimizing all windows"
msgstr "모든 창을 최소화하여 바탕 화면 표시"

#: package/contents/ui/PeekController.qml:13
#, kde-format
msgctxt "@action:button"
msgid "Stop Peeking at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:14
#, kde-format
msgctxt "@action:button"
msgid "Peek at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:16
#, kde-format
msgctxt "@info:tooltip"
msgid "Moves windows back to their original positions"
msgstr ""

#: package/contents/ui/PeekController.qml:17
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Temporarily reveals the Desktop by moving open windows into screen corners"
msgstr ""

#~ msgid "Show Desktop"
#~ msgstr "바탕 화면 표시"

#~ msgid "Show the Plasma desktop"
#~ msgstr "Plasma 바탕 화면 표시"

#~ msgid "Minimize all open windows and show the Desktop"
#~ msgstr "모든 열린 창 최소화 후 데스크톱 보이기"
