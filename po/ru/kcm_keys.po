# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Alexander Yavorsky <kekcuha@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-23 00:48+0000\n"
"PO-Revision-Date: 2021-10-09 17:43+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.2\n"

#: globalaccelmodel.cpp:113
#, kde-format
msgid "Applications"
msgstr "Приложения"

#: globalaccelmodel.cpp:113 globalaccelmodel.cpp:274
#, kde-format
msgid "System Services"
msgstr "Системные службы"

#: globalaccelmodel.cpp:187
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Не удалось сохранить комбинацию для %1: %2"

#: globalaccelmodel.cpp:295
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr ""
"Не удалось добавить «%1», вероятно, это приложение не содержит назначенных "
"действий."

#: globalaccelmodel.cpp:338
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Ошибка обмена данными с глобальной службой комбинаций клавиш"

#: kcm_keys.cpp:50
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Не удалось связаться со службой поддержки глобальных комбинаций клавиш"

#: kcm_keys.cpp:224 package/contents/ui/main.qml:129
#: standardshortcutsmodel.cpp:29
#, kde-format
msgid "Common Actions"
msgstr "Основные действия"

#: kcm_keys.cpp:230
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"Комбинация клавиш «%1» уже назначена действию %3 из основного раздела %2."
"Переназначить?"

#: kcm_keys.cpp:234
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"Комбинация клавиш «%1» уже назначена действию «%2» компонента %3. "
"Переназначить?"

#: kcm_keys.cpp:235
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Конфликт назначения комбинаций клавиш"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "Перед экспортом схемы необходимо сохранить все внесённые изменения"

#: package/contents/ui/main.qml:53
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""
"Выберите все объекты, которые должны быть включены в операцию экспорта схемы"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "Save scheme"
msgstr "Сохранить схему"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Удалить все комбинации клавиш для %1"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Undo deletion"
msgstr "Отменить удаление"

#: package/contents/ui/main.qml:198
#, kde-format
msgid "No items matched the search terms"
msgstr "Не найдено ни одного объекта, удовлетворяющего критериям поиска"

#: package/contents/ui/main.qml:224
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr ""
"Для просмотра назначенных комбинаций \n"
"клавиш выберите объект из списка слева."

#: package/contents/ui/main.qml:232
#, kde-format
msgid "Add Application…"
msgstr "Добавить приложение..."

#: package/contents/ui/main.qml:242
#, kde-format
msgid "Import Scheme…"
msgstr "Импортировать схему..."

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Cancel Export"
msgstr "Отменить экспорт"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Export Scheme…"
msgstr "Экспортировать схему..."

# BUGME: please add 'title:window' context --ayavorsky 20200502
#: package/contents/ui/main.qml:268
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Экспорт схемы комбинаций клавиш"

#: package/contents/ui/main.qml:268 package/contents/ui/main.qml:291
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Импорт схемы комбинаций клавиш"

#: package/contents/ui/main.qml:270
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Схема комбинаций клавиш (*.kksrc)"

#: package/contents/ui/main.qml:296
#, kde-format
msgid "Select the scheme to import:"
msgstr "Выберите файл схемы для импорта:"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Custom Scheme"
msgstr "Пользовательская схема"

#: package/contents/ui/main.qml:313
#, kde-format
msgid "Select File…"
msgstr "Выбрать файл..."

#: package/contents/ui/main.qml:313
#, kde-format
msgid "Import"
msgstr "Импорт"

#: package/contents/ui/ShortcutActionDelegate.qml:26
#, kde-format
msgid "Editing shortcut: %1"
msgstr "Редактирование комбинации: %1"

#: package/contents/ui/ShortcutActionDelegate.qml:38
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ShortcutActionDelegate.qml:51
#, kde-format
msgid "No active shortcuts"
msgstr "Не назначено"

#: package/contents/ui/ShortcutActionDelegate.qml:90
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Комбинации по умолчанию"
msgstr[1] "Комбинации по умолчанию"
msgstr[2] "Комбинации по умолчанию"
msgstr[3] "Комбинация по умолчанию"

#: package/contents/ui/ShortcutActionDelegate.qml:92
#, kde-format
msgid "No default shortcuts"
msgstr "Отсутствуют комбинации по умолчанию"

#: package/contents/ui/ShortcutActionDelegate.qml:100
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "Комбинация клавиш по умолчанию %1 включена."

#: package/contents/ui/ShortcutActionDelegate.qml:100
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "Комбинация клавиш по %1 отключена."

#: package/contents/ui/ShortcutActionDelegate.qml:121
#, kde-format
msgid "Custom shortcuts"
msgstr "Пользовательские комбинации"

#: package/contents/ui/ShortcutActionDelegate.qml:145
#, kde-format
msgid "Delete this shortcut"
msgstr "Удалить эту комбинацию клавиш"

#: package/contents/ui/ShortcutActionDelegate.qml:151
#, kde-format
msgid "Add custom shortcut"
msgstr "Назначить свою комбинацию"

#: package/contents/ui/ShortcutActionDelegate.qml:185
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Отменить назначение комбинации клавиш"

#: standardshortcutsmodel.cpp:33
#, kde-format
msgid "File"
msgstr "Файл"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "Edit"
msgstr "Правка"

#: standardshortcutsmodel.cpp:36
#, kde-format
msgid "Navigation"
msgstr "Навигация"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "View"
msgstr "Просмотр"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "Settings"
msgstr "Настройка"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Help"
msgstr "Справка"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Александр Яворский"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kekcuha@gmail.com"

#~ msgid "Shortcuts"
#~ msgstr "Комбинации клавиш"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"
