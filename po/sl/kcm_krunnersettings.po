# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-02 00:50+0000\n"
"PO-Revision-Date: 2022-07-03 09:10+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 22.04.1\n"

#: package/contents/ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr "Pozicija na zaslonu:"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Top"
msgstr "Vrh"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Center"
msgstr "Center"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr "Aktiviranje:"

#: package/contents/ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr "Aktiviraj ob pritisku katerekoli tipke na namizju"

#: package/contents/ui/main.qml:69
#, kde-format
msgid "History:"
msgstr "Zgodovina:"

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr "Zapomni si pretekla iskanja"

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr "Ohrani zadnje iskanje ob ponovnem odprtju"

#: package/contents/ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr "Zavedajoč se aktivnosti (zadnje iskanje in zgodovina)"

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr "Počisti zgodovino"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr "Počisti zgodovino za aktivnost \"%1\""

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Clear History…"
msgstr "Počisti zgodovino…"

#: package/contents/ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr "Za vse aktivnosti"

#: package/contents/ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr "Za aktivnosti \"%1\""

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr "Vtičniki:"

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Konfiguriraj omogočene vtičnike iskanja…"
