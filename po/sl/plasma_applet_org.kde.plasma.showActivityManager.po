# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jure Repinc <jlp@holodeck1.com>, 2011.
# Andrej Vernekar <andrej.vernekar@gmail.com>, 2011.
# Andrej Mernik <andrejm@ubuntu.si>, 2013.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-20 00:47+0000\n"
"PO-Revision-Date: 2022-07-21 07:49+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.1\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Videz"

#: package/contents/ui/ConfigAppearance.qml:24
#, kde-format
msgid "Icon:"
msgstr "Ikona:"

#: package/contents/ui/ConfigAppearance.qml:26
#, kde-format
msgid "Show the current activity icon"
msgstr "Kaži trenutno ikono dejavnosti"

#: package/contents/ui/ConfigAppearance.qml:32
#, kde-format
msgid "Show the generic activity icon"
msgstr "Kaži generično ikono dejavnosti"

#: package/contents/ui/ConfigAppearance.qml:42
#, kde-format
msgid "Title:"
msgstr "Naslov:"

#: package/contents/ui/ConfigAppearance.qml:44
#, kde-format
msgid "Show the current activity name"
msgstr "Kaži ime trenutne dejavnosti"

#: package/contents/ui/main.qml:73
#, kde-format
msgctxt "@info:tooltip"
msgid "Current activity is %1"
msgstr "Trenutna dejavnosti je %1"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Show Activity Manager"
msgstr "Prikaži upravljalnika dejavnosti"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Click to show the activity manager"
msgstr "Kliknite za prikaz upravljalnika dejavnosti"
