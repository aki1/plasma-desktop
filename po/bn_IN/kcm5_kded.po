# translation of kcmkded.po to Bengali INDIA
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Runa Bhattacharjee <runab@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-04 00:47+0000\n"
"PO-Revision-Date: 2009-01-13 17:47+0530\n"
"Last-Translator: Runa Bhattacharjee <runab@redhat.com>\n"
"Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>\n"
"Language: bn_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "রুণা ভট্টাচার্য্য"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "runab@redhat.com"

#: kcmkded.cpp:48
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Background Services"
msgstr "প্রারম্ভিক পরিসেবা"

#: kcmkded.cpp:52
#, fuzzy, kde-format
#| msgid "(c) 2002 Daniel Molkentin"
msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
msgstr "(c) ২০০২ ড্যানিয়েল মোলকেন্টিন"

#: kcmkded.cpp:53
#, kde-format
msgid "Daniel Molkentin"
msgstr "ড্যানিয়েল মোলকেন্টিন"

#: kcmkded.cpp:54
#, kde-format
msgid "Kai Uwe Broulik"
msgstr ""

#: kcmkded.cpp:125
#, fuzzy, kde-format
#| msgid "Unable to stop server <em>%1</em>."
msgid "Failed to stop service: %1"
msgstr "সার্ভার <em>%1</em> বন্ধ করতে ব্যর্থ।"

#: kcmkded.cpp:127
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service: %1"
msgstr "সার্ভার <em>%1</em> আরম্ভ করতে ব্যর্থ।"

#: kcmkded.cpp:134
#, fuzzy, kde-format
#| msgid "Unable to stop server <em>%1</em>."
msgid "Failed to stop service."
msgstr "সার্ভার <em>%1</em> বন্ধ করতে ব্যর্থ।"

#: kcmkded.cpp:136
#, fuzzy, kde-format
#| msgid "Unable to start server <em>%1</em>."
msgid "Failed to start service."
msgstr "সার্ভার <em>%1</em> আরম্ভ করতে ব্যর্থ।"

#: kcmkded.cpp:234
#, kde-format
msgid "Failed to notify KDE Service Manager (kded5) of saved changed: %1"
msgstr ""

#: package/contents/ui/main.qml:19
#, kde-format
msgid ""
"<p>This module allows you to have an overview of all plugins of the KDE "
"Daemon, also referred to as KDE Services. Generally, there are two types of "
"service:</p> <ul><li>Services invoked at startup</li><li>Services called on "
"demand</li></ul> <p>The latter are only listed for convenience. The startup "
"services can be started and stopped. You can also define whether services "
"should be loaded at startup.</p> <p><b>Use this with care: some services are "
"vital for Plasma; do not deactivate services if you  do not know what you "
"are doing.</b></p>"
msgstr ""

#: package/contents/ui/main.qml:38
#, kde-format
msgid ""
"The background services manager (kded5) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: package/contents/ui/main.qml:47
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded5) was restarted to apply your changes."
msgstr ""

#: package/contents/ui/main.qml:98
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "পরিসেবা"

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "চলমান"

#: package/contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "Not running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "চলমান নয়"

#: package/contents/ui/main.qml:136
#, fuzzy, kde-format
#| msgid "Startup Services"
msgid "Startup Services"
msgstr "প্রারম্ভিক পরিসেবা"

#: package/contents/ui/main.qml:137
#, kde-format
msgid "Load-on-Demand Services"
msgstr "প্রয়োজনে লোড করার জন্য চিহ্নিত পরিসেবা"

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: package/contents/ui/main.qml:221
#, kde-format
msgid "Not running"
msgstr "চলমান নয়"

#: package/contents/ui/main.qml:222
#, kde-format
msgid "Running"
msgstr "চলমান"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "পরিসেবা"

#: package/contents/ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "পরিসেবা"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "KDE Service Manager"
#~ msgstr "KDE Service Manager"

#~ msgid "Status"
#~ msgstr "অবস্থা"

#~ msgid "Description"
#~ msgstr "বিবরণ"

#~ msgid "Use"
#~ msgstr "ব্যবহার"

#~ msgid "Start"
#~ msgstr "আরম্ভ"

#~ msgid "Stop"
#~ msgstr "বন্ধ"

#~ msgid "Unable to contact KDED."
#~ msgstr "KDED-র সাথে সংযোগ স্থাপন করতে ব্যর্থ।"

#~ msgid "Unable to start service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "পরিসেবা <em>%1</em> আরম্ভ করতে ব্যর্থ।<br /><br /><i>ত্রুটি: %2</i>"

#~ msgid "Unable to stop service <em>%1</em>.<br /><br /><i>Error: %2</i>"
#~ msgstr "পরিসেবা <em>%1</em> বন্ধ করতে ব্যর্থ।<br /><br /><i>ত্রুটি: %2</i>"
