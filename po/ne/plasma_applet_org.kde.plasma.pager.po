# English translations for l package.
# Copyright (C) 2007 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
# Automatically generated, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: l 10n-kde4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2007-10-20 05:40+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n !=1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "General:"
msgstr ""

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr ""

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgid "Show only current screen"
msgstr ""

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr ""

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr ""

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr ""

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr ""

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr ""

#: package/contents/ui/configGeneral.qml:80
#, kde-format
msgid "Text display:"
msgstr ""

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr ""

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr ""

#: package/contents/ui/configGeneral.qml:91
#, fuzzy, kde-format
#| msgid "Display the desktop &number"
msgid "Desktop number"
msgstr "Display the desktop &number"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr ""

#: package/contents/ui/configGeneral.qml:99
#, fuzzy, kde-format
#| msgid "Display the desktop &number"
msgid "Desktop name"
msgstr "Display the desktop &number"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current Activity:"
msgstr ""

#: package/contents/ui/configGeneral.qml:113
#, fuzzy, kde-format
#| msgid "Configure Pager"
msgid "Selecting current virtual desktop:"
msgstr "Configure Pager"

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr ""

#: package/contents/ui/configGeneral.qml:124
#, kde-format
msgid "Shows the desktop"
msgstr ""

#: package/contents/ui/main.qml:104
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:362
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:374
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:449
#, fuzzy, kde-format
#| msgid "Display the desktop &number"
msgid "Desktop %1"
msgstr "Display the desktop &number"

#: package/contents/ui/main.qml:450
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr ""

#: package/contents/ui/main.qml:595
#, kde-format
msgid "Show Activity Manager…"
msgstr ""

#: package/contents/ui/main.qml:598
#, fuzzy, kde-format
#| msgid "Configure Pager"
msgid "Add Virtual Desktop"
msgstr "Configure Pager"

#: package/contents/ui/main.qml:599
#, fuzzy, kde-format
#| msgid "Configure Pager"
msgid "Remove Virtual Desktop"
msgstr "Configure Pager"

#: package/contents/ui/main.qml:602
#, fuzzy, kde-format
#| msgid "Configure Pager"
msgid "Configure Virtual Desktops…"
msgstr "Configure Pager"

#, fuzzy
#~| msgid "Configure Pager"
#~ msgid "Configure Desktops..."
#~ msgstr "Configure Pager"

#, fuzzy
#~| msgid "Configure Pager"
#~ msgid "&Remove Last Virtual Desktop"
#~ msgstr "Configure Pager"

#~ msgid "Configure Pager"
#~ msgstr "Configure Pager"

#, fuzzy
#~| msgid "Number of rows:"
#~ msgid "Number of columns:"
#~ msgstr "Number of rows:"

#~ msgid "Number of rows:"
#~ msgstr "Number of rows:"

#~ msgid "Change the number of rows"
#~ msgstr "Change the number of rows"

#~ msgid "Height:"
#~ msgstr "Height:"

#~ msgid "Change the height of the desktop items"
#~ msgstr "Change the height of the desktop items"
