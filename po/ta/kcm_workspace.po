# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Kishore G <kishore96@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-10 00:47+0000\n"
"PO-Revision-Date: 2022-06-21 21:13+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.2\n"

#: package/contents/ui/main.qml:24
#, kde-format
msgid "Visual behavior:"
msgstr "பார்வைமுறை நடத்தை:"

#. i18n: ectx: label, entry (delay), group (PlasmaToolTips)
#: package/contents/ui/main.qml:25 workspaceoptions_plasmasettings.kcfg:9
#, kde-format
msgid "Display informational tooltips on mouse hover"
msgstr "சுட்டியை மேல்வைக்கும்போது கருவித்துப்புகளை காட்டு"

#. i18n: ectx: label, entry (osdEnabled), group (OSD)
#: package/contents/ui/main.qml:37 workspaceoptions_plasmasettings.kcfg:15
#, kde-format
msgid "Display visual feedback for status changes"
msgstr "நிலைமாரும்போது பார்வைமுறை பின்னூட்டம் காட்டு"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Animation speed:"
msgstr "அசைவூட்ட வேகம்:"

#: package/contents/ui/main.qml:78
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "மெதுவானது"

#: package/contents/ui/main.qml:84
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "உடனடியானது"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "கோப்புகள் அல்லது அடைவுகளை க்ளிக் செய்தல்:"

#: package/contents/ui/main.qml:100
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "அவற்றைத் திறக்கும்"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "ஒன்றன் தேர்வுக் குறிப்பை க்ளிக் செய்தல் அதை தேர்ந்தெடுக்கும்"

#: package/contents/ui/main.qml:123
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "அவற்றைத் தேர்ந்தெடுக்கும்"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Open by double-clicking instead"
msgstr "பதிலாக இரட்டை க்ளிக் செய்து திறவுங்கள்"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Clicking in scrollbar track:"
msgstr "உருள் பட்டையில் க்ளிக் செய்தல்:"

#: package/contents/ui/main.qml:152
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls one "
"page up or down'"
msgid "Scrolls one page up or down"
msgstr "ஒரு பக்கம் மேலே/கீழே செல்லும்"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "Middle-click to scroll to clicked location"
msgstr "க்ளிக் செய்யும் இடத்திற்கு செல்ல நடு-க்ளிக் செய்யவும்"

#: package/contents/ui/main.qml:175
#, kde-format
msgctxt ""
"@radio part of a complete sentence: 'Clicking in scrollbar track scrolls to "
"the clicked location'"
msgid "Scrolls to the clicked location"
msgstr "க்ளிக் செய்த இடத்திற்கு செல்லும்"

#: package/contents/ui/main.qml:194
#, kde-format
msgid "Middle Click:"
msgstr "நடு கிளிக்:"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Paste selected text"
msgstr "தேர்ந்தெடுத்துள்ள உரையை ஒட்டும்"

#: package/contents/ui/main.qml:213
#, kde-format
msgid "Touch Mode:"
msgstr "தொடுதல் பயன்முறை:"

#: package/contents/ui/main.qml:215
#, kde-format
msgctxt "As in: 'Touch Mode is automatically enabled as needed'"
msgid "Automatically enable as needed"
msgstr "தேவைக்கேற்ப தானாக இயக்கு"

#: package/contents/ui/main.qml:215 package/contents/ui/main.qml:244
#, kde-format
msgctxt "As in: 'Touch Mode is never enabled'"
msgid "Never enabled"
msgstr "ஒருபோதும் இயக்காதே"

#: package/contents/ui/main.qml:227
#, kde-format
msgid ""
"Touch Mode will be automatically activated whenever the system detects a "
"touchscreen but no mouse or touchpad. For example: when a transformable "
"laptop's keyboard is flipped around or detached."
msgstr ""
"சுட்டியோ தொடுபலகையோ இல்லாமல் தொடுதிரை மட்டும் இருந்தால் தொடுதல் பயன்முறை தானாக "
"இயக்கப்படும். எ.கா. வடிவம் மாறக்கூடிய மடிக்கணினியின் விசைப்பலகை கழற்றப்படும்போதோ "
"மடிக்கப்படும்போதோ."

#: package/contents/ui/main.qml:232
#, kde-format
msgctxt "As in: 'Touch Mode is always enabled'"
msgid "Always enabled"
msgstr "எப்போதும் இயக்கு"

#: package/contents/ui/main.qml:257
#, kde-format
msgid ""
"In Touch Mode, many elements of the user interface will become larger to "
"more easily accommodate touch interaction."
msgstr ""
"தொடுதல் பயன்முறையில், எளிதில் தொடக்கூடிய வித‍த்தில் இடைமுகப்பின் பல உருப்புகள் "
"பெரிதாக்கப்படும்."

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Single click to open files"
msgstr "ஒற்றை க்ளிக் கோப்புகளை திறக்கும்"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:13
#, kde-format
msgid "Animation speed"
msgstr "அசைவூட்ட வேகம்"

#. i18n: ectx: label, entry (scrollbarLeftClickNavigatesByPage), group (KDE)
#: workspaceoptions_kdeglobalssettings.kcfg:17
#, kde-format
msgid "Left-click in scrollbar track moves scrollbar by one page"
msgstr "உருள் பட்டையில் க்ளிக் செய்தல் ஒரு பக்க அளவு நகரும்"

#. i18n: ectx: label, entry (tabletMode), group (Input)
#: workspaceoptions_kwinsettings.kcfg:9
#, kde-format
msgid "Automatically switch to touch-optimized mode"
msgstr "தொடுதல் பயன்முறைக்கு தானாக மாறு"

#. i18n: ectx: label, entry (primarySelection), group (Wayland)
#: workspaceoptions_kwinsettings.kcfg:15
#, kde-format
msgid "Enable middle click selection pasting"
msgstr "நடு கிளிக்கால் தேர்வை ஒட்டவிடு"

#. i18n: ectx: tooltip, entry (osdEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:16
#, kde-format
msgid ""
"Show an on-screen display to indicate status changes such as brightness or "
"volume"
msgstr ""
"பிரகாசம் மற்றும் ஒலி அளவு போன்றவற்றின் நிலை மாறும்போது திரையில் ஓர் அறிகுறி காட்டு"

#. i18n: ectx: label, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:20
#, kde-format
msgid "OSD on layout change"
msgstr "தளவமைப்பு மாறும்போது அறிகுறி காட்டு"

#. i18n: ectx: tooltip, entry (osdKbdLayoutChangedEnabled), group (OSD)
#: workspaceoptions_plasmasettings.kcfg:21
#, kde-format
msgid "Show a popup on layout changes"
msgstr "தளவமைப்பு மாறும்போது தெரித்தெழும் அறிவிப்பு காட்டு"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "கோ. கிஷோர்"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "Kde-l10n-ta@kde.org"

#~ msgid "General Behavior"
#~ msgstr "பொது நடத்தை"

#~ msgid "System Settings module for configuring general workspace behavior."
#~ msgstr "பணிமேடையின் பொது நடத்தையை அமைக்க உதவும் கணினி அமைப்புக் கூறு."

#~ msgid "Furkan Tokac"
#~ msgstr "ஃபுர்க்கான் டொக்காக்கு"

#~ msgid "Never optimize for touch usage"
#~ msgstr "தொடுமுறை பயனுக்கு ஒருபோதும் உகந்த‍தாக்காதே"

#~ msgid "Always optimize for touch usage"
#~ msgstr "தொடுமுறை பயனுக்கு எப்போது உகந்த‍தாக்கு"
