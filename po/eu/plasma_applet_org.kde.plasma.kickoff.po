# Translation for plasma_applet_org.kde.plasma.kickoff.po to Euskara/Basque (eu)
# Copyright (C) 2016, Free Software Foundation.
# Copyright (C) 2017-2022, This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2016, 2017, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2022-10-14 07:02+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Orokorra"

#: package/contents/ui/code/tools.js:49
#, kde-format
msgid "Remove from Favorites"
msgstr "Ezabatu gogokoen artetik"

#: package/contents/ui/code/tools.js:53
#, kde-format
msgid "Add to Favorites"
msgstr "Erantsi gogokoetara"

#: package/contents/ui/code/tools.js:77
#, kde-format
msgid "On All Activities"
msgstr "Jarduera guztietan"

#: package/contents/ui/code/tools.js:127
#, kde-format
msgid "On the Current Activity"
msgstr "Uneko jardueran"

#: package/contents/ui/code/tools.js:141
#, kde-format
msgid "Show in Favorites"
msgstr "Erakutsi gogokoetan"

#: package/contents/ui/ConfigGeneral.qml:37
#, kde-format
msgid "Icon:"
msgstr "Ikonoa:"

#: package/contents/ui/ConfigGeneral.qml:72
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Hautatu..."

#: package/contents/ui/ConfigGeneral.qml:77
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Berrezarri lehenetsitako ikonora"

#: package/contents/ui/ConfigGeneral.qml:83
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr "Kendu ikonoa"

#: package/contents/ui/ConfigGeneral.qml:94
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "Testu-etiketa:"

#: package/contents/ui/ConfigGeneral.qml:96
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "Tekleatu hemen testu-etiketa bat gehitzeko"

#: package/contents/ui/ConfigGeneral.qml:124
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr "Testu etiketa bat ezin da ezarri panela bertikala denean."

#: package/contents/ui/ConfigGeneral.qml:135
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "Orokorra:"

#: package/contents/ui/ConfigGeneral.qml:136
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Aplikazioak beti alfabetikoki sailkatu"

#: package/contents/ui/ConfigGeneral.qml:141
#, kde-format
msgid "Use compact list item style"
msgstr "Erabili zerrenda-trinko elementu estiloa"

#: package/contents/ui/ConfigGeneral.qml:147
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr "Automatikoki ezgaitua ukimen moduan"

#: package/contents/ui/ConfigGeneral.qml:156
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Konfiguratu gaitutako bilaketa pluginak..."

#: package/contents/ui/ConfigGeneral.qml:166
#, kde-format
msgid "Show favorites:"
msgstr "Erakutsi gogokoak:"

#: package/contents/ui/ConfigGeneral.qml:167
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "Sareta batean"

#: package/contents/ui/ConfigGeneral.qml:175
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "Zerrenda batean"

#: package/contents/ui/ConfigGeneral.qml:183
#, kde-format
msgid "Show other applications:"
msgstr "Erakutsi beste aplikazio batzuk:"

#: package/contents/ui/ConfigGeneral.qml:184
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "Sareta batean"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "Zerrenda batean"

#: package/contents/ui/ConfigGeneral.qml:204
#, kde-format
msgid "Show buttons for:"
msgstr "Erakutsi honetarako botoiak:"

#: package/contents/ui/ConfigGeneral.qml:205
#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "Power"
msgstr "Energia"

#: package/contents/ui/ConfigGeneral.qml:214
#, kde-format
msgid "Session"
msgstr "Saioa"

#: package/contents/ui/ConfigGeneral.qml:223
#, kde-format
msgid "Power and session"
msgstr "Energia eta saioa"

#: package/contents/ui/ConfigGeneral.qml:232
#, kde-format
msgid "Show action button captions"
msgstr "Erakutsi ekintza botoiko epigrafeak"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Aplikazioak"

#: package/contents/ui/Footer.qml:110
#, kde-format
msgid "Places"
msgstr "Lekuak"

#: package/contents/ui/FullRepresentation.qml:117
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr "Ez dago bat datorrenik"

#: package/contents/ui/Header.qml:64
#, kde-format
msgid "Open user settings"
msgstr "Ireki erabiltzailearen ezarpenak"

#: package/contents/ui/Header.qml:240
#, kde-format
msgid "Keep Open"
msgstr "Eduki irekita"

#: package/contents/ui/Kickoff.qml:289
#, kde-format
msgid "Edit Applications…"
msgstr "Editatu aplikazioak..."

#: package/contents/ui/KickoffGridView.qml:88
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "%1 errenkada, %2 zutabeko sareta"

#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "Leave"
msgstr "Irten"

#: package/contents/ui/LeaveButtons.qml:71
#, kde-format
msgid "More"
msgstr "Gehiago"

#: package/contents/ui/PlacesPage.qml:49
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Ordenagailua"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Historia"

#: package/contents/ui/PlacesPage.qml:51
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Maiz erabilia"

#~ msgctxt "@item:inmenu Reset icon to default"
#~ msgid "Clear Icon"
#~ msgstr "Berrezarri ikonoa"

#~ msgid "Search…"
#~ msgstr "Bilatu..."

#~ msgid "Primary actions:"
#~ msgstr "Lehen mailako ekintzak:"

#~ msgid "Leave…"
#~ msgstr "Irten…"

#~ msgid "Power…"
#~ msgstr "Energia…"

#~ msgid "More…"
#~ msgstr "Gehiago…"

#~ msgid "Updating applications…"
#~ msgstr "Aplikazioak eguneratzen..."

#~ msgid "Allow labels to have two lines"
#~ msgstr "Etiketek bi lerro izatea baimentzen du"

#~ msgid "%2@%3 (%1)"
#~ msgstr "%2@%3 (%1)"

#~ msgid "%1@%2"
#~ msgstr "%1@%2"

#~ msgid "List with 1 item"
#~ msgid_plural "List with %1 items"
#~ msgstr[0] "Elementu bakarreko zerrenda"
#~ msgstr[1] "%1 elementuko zerrenda"

#~ msgid "%1 submenu"
#~ msgstr "%1 azpimenua"

#~ msgid "Leave..."
#~ msgstr "Irten..."

#~ msgid "Power..."
#~ msgstr "Energia..."

#~ msgid "More..."
#~ msgstr "Gehiago..."

#~ msgid "Applications updated."
#~ msgstr "Eguneratutako aplikazioak."

#~ msgid "Switch tabs on hover"
#~ msgstr "Fitxa aldatu gainetik pasatzean"

#~ msgid "All Applications"
#~ msgstr "Aplikazio guztiak"

#~ msgid "Favorites"
#~ msgstr "Gogokoak"

#~ msgid "Often Used"
#~ msgstr "Maiz erabilia"

#~ msgid "Active Tabs"
#~ msgstr "Fitxa aktiboak"

#~ msgid "Inactive Tabs"
#~ msgstr "Fitxa ez-aktiboak"

#~ msgid ""
#~ "Drag tabs between the boxes to show/hide them, or reorder the visible "
#~ "tabs by dragging."
#~ msgstr ""
#~ "Arrastatu fitxak koadro batetik bestera haiek erakutsi/ezkutatzeko, edo "
#~ "fitxa ikusgaien ordena aldatu haiek arrastatuz."

#~ msgid "Expand search to bookmarks, files and emails"
#~ msgstr ""
#~ "Zabaldu bilaketa laster-marketara, fitxategietara eta posta-"
#~ "elektronikoetara"

#~ msgid "Appearance"
#~ msgstr "Itxura"

#~ msgid "Menu Buttons"
#~ msgstr "Menuko botoiak"

#~ msgid "Visible Tabs"
#~ msgstr "Fitxa ikusgarriak"
