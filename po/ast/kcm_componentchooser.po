# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# enolp <enolp@softastur.org>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-09 00:48+0000\n"
"PO-Revision-Date: 2021-01-20 23:04+0100\n"
"Last-Translator: enolp <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.1\n"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr ""

#: componentchooser.cpp:74 componentchooserterminal.cpp:74
#, kde-format
msgid "Other…"
msgstr ""

#: componentchooserbrowser.cpp:18
#, kde-format
msgid "Select default browser"
msgstr ""

#: componentchooseremail.cpp:19
#, kde-format
msgid "Select default e-mail client"
msgstr ""

#: componentchooserfilemanager.cpp:14
#, kde-format
msgid "Select default file manager"
msgstr ""

#: componentchoosergeo.cpp:11
#, kde-format
msgid "Select default map"
msgstr ""

#: componentchoosertel.cpp:15
#, fuzzy, kde-format
#| msgid "Select preferred Web browser application:"
msgid "Select default dialer application"
msgstr "Esbilla l'aplicación de restolador web preferida:"

#: componentchooserterminal.cpp:24
#, kde-format
msgid "Select default terminal emulator"
msgstr ""

#: package/contents/ui/main.qml:30
#, kde-format
msgid "Web browser:"
msgstr ""

#: package/contents/ui/main.qml:42
#, kde-format
msgid "File manager:"
msgstr ""

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Email client:"
msgstr ""

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Terminal emulator:"
msgstr ""

#: package/contents/ui/main.qml:78
#, kde-format
msgid "Map:"
msgstr ""

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Softastur"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alministradores@softastur.org"

#~ msgid "Joseph Wenninger"
#~ msgstr "Joseph Wenninger"

#~ msgid "Unknown"
#~ msgstr "Desconozse"

#~ msgid "(c), 2002 Joseph Wenninger"
#~ msgstr "(c), 2002 Joseph Wenninger"

#~ msgid ""
#~ "<qt>You changed the default component of your choice, do want to save "
#~ "that change now ?</qt>"
#~ msgstr ""
#~ "<qt>Camudesti'l componente predetermináu de la escoyeta, ¿quies guardar "
#~ "el cambéu agora?</qt>"

#~ msgid "No description available"
#~ msgstr "Nun hai una descripción disponible"

#~ msgid "Default Component"
#~ msgstr "Componente predetermináu"

#~ msgid "<qt>Open <b>http</b> and <b>https</b> URLs</qt>"
#~ msgstr "<qt>Abrir les URLs en <b>http</b> y <b>https</b></qt>"

#~ msgid "in an application based on the contents of the URL"
#~ msgstr "nuna aplicación según el conteníu de la URL"

#~ msgid "in the following application:"
#~ msgstr "na aplicación de darréu:"

#~ msgid "with the following command:"
#~ msgstr "col comandu de darréu:"

#~ msgid "..."
#~ msgstr "…"

#~ msgid "&Use KMail as preferred email client"
#~ msgstr "&Usar KMail como'l veceru preferíu de corréu"

#~ msgid "Select this option if you want to use any other mail program."
#~ msgstr "Esbilla esta opción si quies usar otru programa de corréu."

#~ msgid "Click here to browse for the mail program file."
#~ msgstr "Calca equí pa restolar un programa de corréu."

#~ msgid ""
#~ "Activate this option if you want the selected email client to be executed "
#~ "in a terminal (e.g. <em>Konsole</em>)."
#~ msgstr ""
#~ "Activa esta opción si quies que'l veceru de corréu s'execute nuna "
#~ "terminal (por exemplu <em>Konsole</em>)."

#~ msgid "&Run in terminal"
#~ msgstr "&Executar na terminal"

#~ msgid "Browse directories using the following file manager:"
#~ msgstr "Restolar direutorios usando'l xestor de ficheros de darréu:"

#~ msgid "&Use Konsole as terminal application"
#~ msgstr "&Usar Konsole como l'aplicación de la terminal"

#~ msgid "Use a different &terminal program:"
#~ msgstr "Usar un programa de terminal diferente:"

#~ msgid "Click here to browse for terminal program."
#~ msgstr "Calca equí pa restolar un programa de terminal."
