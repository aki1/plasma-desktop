# translation of kcmsmserver.po to Uzbek
# translation of kcmsmserver.po to
# Copyright (C) 2003, 2004, 2005 Free Software Foundation, Inc.
# Mashrab Kuvatov <kmashrab@uni-bremen.de>, 2003, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmsmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2005-10-11 20:31+0200\n"
"Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>\n"
"Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>\n"
"Language: uz@cyrillic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: kcmsmserver.cpp:49
#, kde-format
msgid ""
"<h1>Session Manager</h1> You can configure the session manager here. This "
"includes options such as whether or not the session exit (logout) should be "
"confirmed, whether the session should be restored again when logging in and "
"whether the computer should be automatically shut down after session exit by "
"default."
msgstr ""

#: package/contents/ui/main.qml:25
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr ""

#: package/contents/ui/main.qml:26
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""

#: package/contents/ui/main.qml:27
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""

#: package/contents/ui/main.qml:32
#, fuzzy, kde-format
#| msgid "&Restart computer"
msgid "Restart Now"
msgstr "К&омпьютерни ўчириб-ёқиш"

#: package/contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "General"
msgid "General:"
msgstr "Умумий"

#: package/contents/ui/main.qml:39
#, fuzzy, kde-format
#| msgid "Conf&irm logout"
msgctxt "@option:check"
msgid "Confirm logout"
msgstr "Чиқишни &тасдиқлаш"

#: package/contents/ui/main.qml:48
#, fuzzy, kde-format
#| msgid "O&ffer shutdown options"
msgctxt "@option:check"
msgid "Offer shutdown options"
msgstr "Ўчириш турларини &таклиф қилиш"

#: package/contents/ui/main.qml:65
#, fuzzy, kde-format
#| msgid "Default Shutdown Option"
msgid "Default leave option:"
msgstr "Андоза ўчириш тури"

#: package/contents/ui/main.qml:66
#, fuzzy, kde-format
#| msgid "&End current session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "End current session"
msgstr "&Жорий сеансни тугатиш"

#: package/contents/ui/main.qml:76
#, fuzzy, kde-format
#| msgid "&Restart computer"
msgctxt "@option:radio"
msgid "Restart computer"
msgstr "К&омпьютерни ўчириб-ёқиш"

#: package/contents/ui/main.qml:86
#, fuzzy, kde-format
#| msgid "&Turn off computer"
msgctxt "@option:radio"
msgid "Turn off computer"
msgstr "Ко&мпьютерни ўчириш"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "When logging in:"
msgstr ""

#: package/contents/ui/main.qml:103
#, fuzzy, kde-format
#| msgid "Restore &manually saved session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last session"
msgstr "Қўлбола &сақланган сенсларни таклаш"

#: package/contents/ui/main.qml:113
#, fuzzy, kde-format
#| msgid "Restore &manually saved session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Restore last manually saved session"
msgstr "Қўлбола &сақланган сенсларни таклаш"

#: package/contents/ui/main.qml:123
#, fuzzy, kde-format
#| msgid "Start with an empty &session"
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr "&Бўш сеанс билан бошлаш"

#: package/contents/ui/main.qml:135
#, kde-format
msgid "Don't restore these applications:"
msgstr ""

#: package/contents/ui/main.qml:152
#, fuzzy, kde-format
#| msgid ""
#| "Here you can enter a comma-separated list of applications that should not "
#| "be saved in sessions, and therefore will not be started when restoring a "
#| "session. For example 'xterm,xconsole'."
msgid ""
"Here you can enter a colon or comma separated list of applications that "
"should not be saved in sessions, and therefore will not be started when "
"restoring a session. For example 'xterm:konsole' or 'xterm,konsole'."
msgstr ""
"Бу ерда вергул билан ажратилган дастурларнинг номини киритиш мумкин. Улар "
"сеансни тиклаганда ишга туширилмайди. Масалан, 'xterm,xconsole'."

#: package/contents/ui/main.qml:161
#, kde-format
msgctxt "@option:check"
msgid "Enter UEFI setup screen on next restart"
msgstr ""

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "@option:check"
msgid "Enter firmware setup screen on next restart"
msgstr ""

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, fuzzy, kde-format
#| msgid "Conf&irm logout"
msgid "Confirm logout"
msgstr "Чиқишни &тасдиқлаш"

#. i18n: ectx: label, entry (offerShutdown), group (General)
#: smserversettings.kcfg:13
#, fuzzy, kde-format
#| msgid "O&ffer shutdown options"
msgid "Offer shutdown options"
msgstr "Ўчириш турларини &таклиф қилиш"

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:17
#, fuzzy, kde-format
#| msgid "Default Shutdown Option"
msgid "Default leave option"
msgstr "Андоза ўчириш тури"

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:26
#, fuzzy, kde-format
#| msgid "On Login"
msgid "On login"
msgstr "Тизимга киришда"

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:30
#, fuzzy, kde-format
#| msgid "Applications to be e&xcluded from sessions:"
msgid "Applications to be excluded from session"
msgstr "Сеансга &кирмаган дастурлар:"

#, fuzzy
#~| msgid "Restore &previous session"
#~ msgid "Desktop Session"
#~ msgstr "&Олдинги сеансни қайта тиклаш"

#, fuzzy
#~| msgid "Restore &manually saved session"
#~ msgid "Restore previous saved session"
#~ msgstr "Қўлбола &сақланган сенсларни таклаш"

#~ msgid ""
#~ "Check this option if you want the session manager to display a logout "
#~ "confirmation dialog box."
#~ msgstr "Агар белгиланса, сеансни тугатишда тасдиқлаш учун ойна кўринади."

#~ msgid "Conf&irm logout"
#~ msgstr "Чиқишни &тасдиқлаш"

#~ msgid "O&ffer shutdown options"
#~ msgstr "Ўчириш турларини &таклиф қилиш"

#~ msgid ""
#~ "Here you can choose what should happen by default when you log out. This "
#~ "only has meaning, if you logged in through KDM."
#~ msgstr ""
#~ "Бу ерда чиқишдан сўнг қайси амал бажарилишини танлашингиз мумкин. Бу "
#~ "тизимга фақат KDM орқали кирилса маънога эга."

#, fuzzy
#~| msgid "Default Shutdown Option"
#~ msgid "Default Leave Option"
#~ msgstr "Андоза ўчириш тури"

#~ msgid "On Login"
#~ msgstr "Тизимга киришда"

#~ msgid "Applications to be e&xcluded from sessions:"
#~ msgstr "Сеансга &кирмаган дастурлар:"

#~ msgid "Session Manager"
#~ msgstr "Сеанс бошқарувчиси"

#~ msgid "Advanced"
#~ msgstr "Қўшимча"

#, fuzzy
#~| msgid "Session Manager"
#~ msgid "Window Manager"
#~ msgstr "Сеанс бошқарувчиси"
