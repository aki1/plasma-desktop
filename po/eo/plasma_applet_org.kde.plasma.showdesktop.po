# Translation of plasma_applet_showdesktop into esperanto.
# Axel Rousseau <axel@esperanto-jeunes.org>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_showdesktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2009-11-15 12:06+0100\n"
"Last-Translator: Axel Rousseau <axel@esperanto-jeunes.org>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: pology\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/MinimizeAllController.qml:17
#, kde-format
msgctxt "@action:button"
msgid "Restore All Minimized Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:18
#, kde-format
msgctxt "@action:button"
msgid "Minimize All Windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:20
#, kde-format
msgctxt "@info:tooltip"
msgid "Restores the previously minimized windows"
msgstr ""

#: package/contents/ui/MinimizeAllController.qml:21
#, kde-format
msgctxt "@info:tooltip"
msgid "Shows the Desktop by minimizing all windows"
msgstr ""

#: package/contents/ui/PeekController.qml:13
#, kde-format
msgctxt "@action:button"
msgid "Stop Peeking at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:14
#, kde-format
msgctxt "@action:button"
msgid "Peek at Desktop"
msgstr ""

#: package/contents/ui/PeekController.qml:16
#, kde-format
msgctxt "@info:tooltip"
msgid "Moves windows back to their original positions"
msgstr ""

#: package/contents/ui/PeekController.qml:17
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Temporarily reveals the Desktop by moving open windows into screen corners"
msgstr ""
