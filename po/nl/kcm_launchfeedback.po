# translation of kcmlaunch.po to Nederlands
# Nederlandse vertaling van kcmlaunch
# Copyright (C) 2000-2002 KDE e.v..
# KDE-vertaalgroep Nederlands <www.kde.nl>
# proefgelezen door Chris Hooijer (cr.hooijer@hccnet.nl) op 24-04-2002
# Rinse de Vries <Rinse@kde.nl>, 2000-2002.
# Rinse de Vries <rinse@kde.nl>, 2003, 2004.
# Wilbert Berendsen <wbsoft@xs4all.nl>, 2003.
# Sander Koning <sanderkoning@kde.nl>, 2005.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2018, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcmlaunch\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2021-05-27 15:17+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettingsbase.kcfg:15 launchfeedbacksettingsbase.kcfg:29
#, kde-format
msgid "Timeout in seconds"
msgstr "Timeout in seconden"

#: package/contents/ui/main.qml:17
#, kde-format
msgid "Launch Feedback"
msgstr "Terugkoppeling van opstarten"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Cursor:"
msgstr "Cursor:"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "No Feedback"
msgstr "Geen terugkoppeling"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Static"
msgstr "Statisch"

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Blinking"
msgstr "Knipperen"

#: package/contents/ui/main.qml:79
#, kde-format
msgid "Bouncing"
msgstr "Stuiteren"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Task Manager:"
msgstr "Takenbeheerder:"

#: package/contents/ui/main.qml:99
#, kde-format
msgid "Enable animation"
msgstr "Animatie inschakelen"

#: package/contents/ui/main.qml:113
#, kde-format
msgid "Stop animation after:"
msgstr "Animatie stoppen na:"

#: package/contents/ui/main.qml:126 package/contents/ui/main.qml:138
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 seconde"
msgstr[1] "%1 seconden"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - t/m 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "Configure application launch feedback"
#~ msgstr "Terugkoppeling opstarten van toepassing configureren"

#~ msgid ""
#~ "<h1>Launch Feedback</h1> You can configure the application-launch "
#~ "feedback here."
#~ msgstr ""
#~ "<h1>Opstartnotificatie</h1>Hier kunt u de wijze waarop u gewezen wordt op "
#~ "een startend programma instellen."

#~ msgid ""
#~ "<h1>Busy Cursor</h1>\n"
#~ "KDE offers a busy cursor for application startup notification.\n"
#~ "To enable the busy cursor, select one kind of visual feedback\n"
#~ "from the combobox.\n"
#~ "It may occur, that some applications are not aware of this startup\n"
#~ "notification. In this case, the cursor stops blinking after the time\n"
#~ "given in the section 'Startup indication timeout'"
#~ msgstr ""
#~ "<h1>Bezig-cursor</h1>\n"
#~ "KDE levert een bezig-cursor als notificatie voor opstartende "
#~ "programma's.\n"
#~ "Om de bezig-cursor te activeren, selecteer en van de opties in het "
#~ "combinatieveld.\n"
#~ "Het kan voorkomen dat bepaalde programma's zich niet bewust zijn van\n"
#~ "deze opstartnotificatie. In dat geval stopt de cursor met knipperen\n"
#~ "na het tijdsbestek dat is opgegeven in de sectie\n"
#~ "\"Tijdslimiet opstartindicatie\"."

#~ msgid "No Busy Cursor"
#~ msgstr "Geen bezig-cursor"

#~ msgid "Passive Busy Cursor"
#~ msgstr "Passieve bezig-cursor"

#~ msgid "Taskbar &Notification"
#~ msgstr "Taakbalk&notificatie"

#~ msgid ""
#~ "<H1>Taskbar Notification</H1>\n"
#~ "You can enable a second method of startup notification which is\n"
#~ "used by the taskbar where a button with a rotating hourglass appears,\n"
#~ "symbolizing that your started application is loading.\n"
#~ "It may occur, that some applications are not aware of this startup\n"
#~ "notification. In this case, the button disappears after the time\n"
#~ "given in the section 'Startup indication timeout'"
#~ msgstr ""
#~ "<H1>Taakbalknotificatie</H1>\n"
#~ "U kunt een tweede methode activeren voor de opstartnotificatie. Deze\n"
#~ "notificatie wordt gebruikt door de taakbalk, waar dan een zandloper\n"
#~ "verschijnt. Deze geeft aan dat de opgestarte toepassing wordt geladen.\n"
#~ "Het kan gebeuren dat bepaalde toepassingen zich niet bewust zijn van "
#~ "deze\n"
#~ "opstartnotificatie. In dat geval verdwijnt de knop na het tijdsbestek dat "
#~ "is\n"
#~ " opgegeven in de sectie \"Tijdslimiet opstartindicatie\"."

#~ msgid "Start&up indication timeout:"
#~ msgstr "Tijdslimiet op&startindicatie:"
